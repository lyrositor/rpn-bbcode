# rpn-bbcode

This is a collection of BBCode resources which can be used on [RPNation](https://www.rpnation.com/). Feel free to use them in your own posts.

*License:* [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/)